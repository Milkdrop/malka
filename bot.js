const fs = require('fs');
const Discord = require('discord.js');
require('discord-reply');
const client = new Discord.Client();
const gTTS = require('gtts');
const { Readable } = require('stream');
const { spawn } = require('child_process');
const { exec } = require("child_process");
const ConversationalUnit = require('./cores/knowledge-core.js');
var mp3Duration = require('mp3-duration');

class Silence extends Readable {
    _read() {
        this.push(Buffer.from([0xF8, 0xFF, 0xFE]));
        this.destroy();
    }
}

const recognizer = spawn('python3', ['recognizer.py'], {stdio: ['pipe', 'pipe', 'pipe']});

recognizer.stderr.on('data', (data) => { console.log("[STDERR] Recognizer: " + data.toString()); });
recognizer.stdout.on('data', async (data) => {

	data = data.toString();

	if (!botVoiceChannelConnection || data.split(":").length < 2) {
		process.stdout.write(data + "\n");
		return;
	}

	console.log("Data:");
	console.log(data);
	
	id = data.split(":")[0];
	data = data.substring(data.indexOf(":") + 1);
	data = data.substring(0, data.indexOf("\n")).trim();

	var user = await client.users.fetch(id);

	console.log(id + " (" + user.username + "): " + data);

	response = await processVoiceInput(data, botVoiceChannelConnection.channel.id, user.username);

	if (response.length > 0) {
		speakInVoice(response);
	}
});

botVoiceChannelConnection = null;
infiltrateChannel = null;
audioThreads = {};
conversationalUnits = {};
usernames = {};
userFocus = null;
bannedUids = [];

isProcessing = false;
playedFirstAwake = false;

function getRandomChoice(arr) {
	return arr[Math.floor(Math.random() * arr.length)];
}

function parseUid(uid) {
	if (uid.indexOf("<") != -1) {
		uid = uid.substring(2, uid.length - 1);
		if (uid.indexOf("!") != -1) {
			uid = uid.substring(1);
		}
	}

	return uid;
}

function speakInVoice(text) {
	filename = "./tmp/" + botVoiceChannelConnection.channel.id

	try {
		var gtts = new gTTS(text.replace(/GLaDOS/g, "glados"), 'en');
		gtts.save(filename + ".mp3", function (err, result) {
			if (err) { 
				console.log("Error while accessing GTTS");
				speakInVoice(text);
			} else {
				exec("sox --norm -- " + filename + ".mp3 " + filename + "-sox.mp3 pitch -150 flanger 30 10 15 100 0.1 flanger 10 8 55 100 0.1 bass 20", (error, stdout, stderr) => {
					botVoiceChannelConnection.play(filename + "-sox.mp3");

					mp3Duration(filename + "-sox.mp3", function (err, duration) {
						console.log(duration);
						const timeoutObj = setTimeout(() => {
							isProcessing = false;
						}, duration * 1000 - 500);
					});
				});
			}
		});
	} catch (e) {
		console.log("Error while accessing GTTS");
		speakInVoice(text);
	}
}

function registerNewUserThread(uid) {
	if (bannedUids.includes(uid.toString())) {
		console.log("ignoring " + uid.toString());
		return;
	}

	const audio = botVoiceChannelConnection.receiver.createStream(uid, { mode: 'pcm', end: 'manual' });

	uid = uid.padStart(18, '0');
	console.log("Registered new user: " + uid.toString());

	audio.on("data", (data) => {
		buf = Buffer.concat([Buffer.from(uid), Buffer.from('\0'), data]);
		if (buf.length != 3859) { console.log("CRITICAL!!! Buffer length not 3859."); }
		recognizer.stdin.write(buf);
	});

	audioThreads[uid] = audio;
}

function removeUserThread(uid) {
	uid = uid.padStart(18, '0');
	console.log("Unregistered user: " + uid.toString());

	if (audioThreads[uid]) audioThreads[uid].on("data", (data) => {});
	delete audioThreads[uid];
}

function createCUIfNotExists(chId, authorName) {
	if (!conversationalUnits[chId]) {
		conversationalUnits[chId] = new ConversationalUnit(authorName);
	}
}

function stopEverything() {
	for (const [key, value] of Object.entries(audioThreads)) {
		value.on("data", (data) => {});
	}

	recognizer.stdin.write("COMMAND_STOP".padEnd(3859, '\0'));
	audioThreads = {};

	if (botVoiceChannelConnection) botVoiceChannelConnection.disconnect();
	botVoiceChannelConnection = null;
}

async function processVoiceInput(input, channelId, username, bypassFocus = false) {
	createCUIfNotExists(channelId, username);
	conversationalUnits[channelId].setSize(200);
	conversationalUnits[channelId].setEngine("INFERKIT");

	prefix = "computer";
	if (input.indexOf(prefix) !== -1) {
		tempInput = input.substring(input.indexOf(prefix) + prefix.length).trim().toLowerCase()
		args = tempInput.split(" ")

		switch (args[0]) {
			case "play": {
				musicName = tempInput.substring(tempInput.indexOf("play") + 5);
				console.log("should play moosic " + musicName);

				speakInVoice("Playing " + musicName);
				await infiltrateChannel.send(";;p1 " + musicName);
			} return "";
			case "skip": if (args[1] == "music") await infiltrateChannel.send(";;skip"); return "";
			case "pause": if (args[1] == "music") await infiltrateChannel.send(";;pauza"); return "";
			case "continue": if (args[1] == "music") await infiltrateChannel.send(";;continua"); return "";
			case "repeat": if (args[1] == "music") await infiltrateChannel.send(";;repeta"); return "";
			case "stop": {
				if (args[1] == "music") await infiltrateChannel.send(";;stop");
				else {
					userFocus = null;
					speakInVoice("Ok.");
				}
			} return "";

			case "reset": {
				conversationalUnits[channelId].resetConversation(true);
				speakInVoice("Memory has been wiped.");
				if (isProcessing) {
					isProcessing = false;
				}
			} return "";

			// focus user
			case "listen": {
				userFocus = username;
				botVoiceChannelConnection.play("./awake.mp3");
			} return "";

			// one shot
			case "hey": case "hi": {
				return await processVoiceInput(args.slice(1).join(" ").trim(), channelId, username, true);
			} return "";

			default: if (input.startsWith(prefix)) botVoiceChannelConnection.play("./invalid.mp3"); break;
		}
	}

	if (isProcessing) {
		console.log("Voice input: " + username + " " + input + " (ignored while processing)");
		return "";
	}

	if (input.length > 0 && (userFocus === username || bypassFocus)) {
		console.log("[FOCUS" + (bypassFocus ? "-BYPASS":"") + "] Voice input: " + username + " " + input);
		isProcessing = true;
		return await conversationalUnits[channelId].requestResponse(username, input);
	} else {
		console.log("Voice input: " + username + " " + input + " (not focused)");
		return "";
	}
}

async function processTextInput(input, channelId, username, messageChannel, userVoiceChannel) {
	args = input.split(/\s/);

	if (channelId == infiltrateChannel && botVoiceChannelConnection) {
		channelId = botVoiceChannelConnection.channel.id;
	}

	createCUIfNotExists(channelId, username);

	response = "**FAIL.**";

	switch (args[0]) {
		case "AUDIO_INFILTRATE": if (userVoiceChannel) {
			infiltrateChannel = messageChannel;
			botVoiceChannelConnection = await userVoiceChannel.join();
			botVoiceChannelConnection.play(new Silence(), { type: 'opus' });
			userVoiceChannel.members.each(member => {registerNewUserThread(member.id); usernames[member.id] = member.username;});

			if (!playedFirstAwake) {
				botVoiceChannelConnection.play("./invalid.mp3", { volume: 0 });
				botVoiceChannelConnection.play("./awake.mp3", { volume: 0 });
				playedFirstAwake = true;
			}

			response = "**OK.**";
		} break;

		case "AUDIO_EVACUATE": {
			stopEverything();
			response = "**OK.**";
		} break;

		case "AUDIO_IGNORE": {
			uid = parseUid(args[1]);

			bannedUids.push(uid);
			removeUserThread(uid);
			recognizer.stdin.write(("COMMAND_IGNORE_ID " + uid).padEnd(3859, '\0'));
			response = "**OK.**";
		} break;

		case "AUDIO_CLEAR_IGNORE": {
			bannedUids = [];
			recognizer.stdin.write(("COMMAND_CLEAR_IGNORE").padEnd(3859, '\0'));
			response = "**OK.**";
		} break

		case "CONVERSATION_RESET": {
			conversationalUnits[channelId].resetConversation(true);
			response = "**OK.**";
		} break;
		
		case "PERSONALITY_CORE": {
			var personality = args[1];
			if (conversationalUnits[channelId].setPersonalityCore(args[1])) {
				response = "**OK.**";
			}
		} break;
		
		case "GEN": {
			input = input.substring("GEN".length + 1);
			
			response = await conversationalUnits[channelId].requestResponse(username, input, true);
		} break;
		
		case "ASK": {
			input = input.substring("ASK".length + 1);
			
			conversationalUnits[channelId].setPersonalityCore("CATGIRL_ASK");
			response = await conversationalUnits[channelId].requestResponse(username, input);
		} break;
		
		case "GASK": {
			input = input.substring("GASK".length + 1);
			
			conversationalUnits[channelId].setPersonalityCore("GLADOS_ASK");
			response = await conversationalUnits[channelId].requestResponse(username, input);
		} break;

		case "CORE_TEMPERATURE": {
			var temperature = parseFloat(args[1]);
			if (conversationalUnits[channelId].setTemperature(temperature)) {
				response = "**OK.**";
			}
		} break;
		
		case "CORE_SIZE": {
			var size = parseInt(args[1]);
			if (conversationalUnits[channelId].setSize(size)) {
				response = "**OK.**";
			}
		} break;
		
		case "CORE_SET": {
			if (conversationalUnits[channelId].setEngine(args[1])) {
				response = "**OK.**";
			}
		} break;

		default: {
			response = await conversationalUnits[channelId].requestResponse(username, input);
		}
	}

	return response;
}

client.on('ready', () => {
	console.log("Bot started.");
});

client.on('voiceStateUpdate', (oldMember, newMember) => {
	if (oldMember.channelID != newMember.channelID) {
		if (botVoiceChannelConnection) {
			botChannelId = botVoiceChannelConnection.channel.id;

			if (oldMember.channelID !== botChannelId && newMember.channelID === botChannelId) {
				botVoiceChannelConnection.play(new Silence(), { type: 'opus' });
				registerNewUserThread(newMember.id);
				usernames[newMember.id] = newMember.username;
			}

			if (oldMember.channelID === botChannelId && newMember.channelID !== botChannelId) {
				removeUserThread(newMember.id);
			}

			if (newMember.id == client.user.id) {
				console.log("We have been moved or disconnected! Abort.");
				stopEverything();
			}
		}
	}
});

client.on('message', async message => {
	prefix = "^"

	if (!message.content.startsWith(prefix)) {
		return;
	}

	input = message.content.substring(prefix.length);
	response = await processTextInput(input, message.channel.id, message.author.username, message.channel, message.member.voice.channel);

	if (response.length > 0) {
		if (response.length >= 2000) {
			response = response.substring(response.length - 2000);
		}

		message.lineReplyNoMention(response);
	} else {
		message.lineReplyNoMention("**CORE CONFUSED.**");
	}
});

tmpDir = "tmp";
if (!fs.existsSync(tmpDir)) {
	fs.mkdirSync(tmpDir);
}

fs.readFile("./token", "ascii", function(err, data) {
	if (err) { return console.log(err); }
	client.login(data.trim());
})