#
# This file is part of Dragonfly.
# (c) Copyright 2019 by David Zurow
# Licensed under the LGPL.
#
#   Dragonfly is free software: you can redistribute it and/or modify it
#   under the terms of the GNU Lesser General Public License as published
#   by the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   Dragonfly is distributed in the hope that it will be useful, but
#   WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#   Lesser General Public License for more details.
#
#   You should have received a copy of the GNU Lesser General Public
#   License along with Dragonfly.  If not, see
#   <http://www.gnu.org/licenses/>.
#

"""
Audio input/output classes for Kaldi backend
"""

import collections, itertools, time
import sys, os, subprocess, threading
import webrtcvad, kaldi_active_grammar

# Disable recognizer for now
while True:
    time.sleep(1)

discordFrameLengthInBytes = int(160 * 2 * (48000 / 16000) * 2) # 16bit, 48000 Hz, 2 channels, sample width 2

class UserBuffer():

    def __init__(self):
        self.vad = webrtcvad.Vad(3)

        self.buffer = b""
        self.bufferPointer = 0
        self.blockStack = []

        self.generator = self.internalBlockGenerator()

    def appendData(self, data):
        self.buffer += data

        if (self.bufferPointer != 0):
            self.buffer = self.buffer[self.bufferPointer:]
            self.bufferPointer = 0

    def getNextBlock(self):
        return next(self.generator)

    def getBufferSize(self):
        return len(self.buffer)

    def clearBuffer(self):
        self.buffer = b""
        self.bufferPointer = 0
        self.blockStack = []

        self.generator = self.internalBlockGenerator()

    def internalBlockGenerator(self, start_window_ms=150, start_padding_ms=100,
        end_window_ms=1000, end_padding_ms=None, complex_end_window_ms=None,
        ratio=0.8, blocks=None, nowait=False, audio_auto_reconnect=False,
        ):
        """Generator/coroutine that yields series of consecutive audio blocks comprising each phrase, separated by yielding a single None.
            Determines voice activity by ratio of blocks in window_ms. Uses a buffer to include window_ms prior to being triggered.
            Example: (block, ..., block, None, block, ..., block, None, ...)
                      |----phrase-----|        |----phrase-----|
        """
        assert end_padding_ms == None, "end_padding_ms not supported yet"
        num_start_window_blocks = max(1, int(start_window_ms // 10))
        num_start_padding_blocks = max(0, int((start_padding_ms or 0) // 10))
        num_end_window_blocks = max(1, int(end_window_ms // 10))
        num_complex_end_window_blocks = max(1, int((complex_end_window_ms or end_window_ms) // 10))
        num_end_padding_blocks = max(0, int((end_padding_ms or 0) // 10))
        audio_reconnect_threshold_blocks = 5
        audio_reconnect_threshold_time = 50 * 10 / 1000

        ring_buffer = collections.deque(maxlen=max(
            (num_start_window_blocks + num_start_padding_blocks),
            (num_end_window_blocks + num_end_padding_blocks),
            (num_complex_end_window_blocks + num_end_padding_blocks),
        ))
        ring_buffer_recent_slice = lambda num_blocks: itertools.islice(ring_buffer, max(0, (len(ring_buffer) - num_blocks)), None)

        triggered = False
        in_complex_phrase = False
        last_good_block_time = time.time()

        while True:
            if (len(self.blockStack) == 0):
                try:
                    manualSilence = False

                    if (self.bufferPointer == len(self.buffer)):
                        self.buffer = b""
                        self.bufferPointer = 0
                        manualSilence = True
                    else:
                        giveup = 0
                        while (len(self.buffer) < discordFrameLengthInBytes or self.bufferPointer != 0):
                            giveup += 1
                            if (giveup >= 500):
                                manualSilence = True
                                #print(f"Giving up on waiting. Buffer size: {len(self.buffer)}. Buffer pointer: {self.bufferPointer}", flush = True)
                                break

                            time.sleep(0.001)

                    if (manualSilence):
                        self.blockStack.append(b'\0' * 160 * 2)
                    else:
                        blocksAvailable = len(self.buffer) // discordFrameLengthInBytes
                        soxInput = self.buffer[:discordFrameLengthInBytes * blocksAvailable]
                        self.bufferPointer = discordFrameLengthInBytes * blocksAvailable

                        #print("Input length: " + str(len(soxInput)))

                        soxcmd = 'sox -r 48000 --bits 16 --endian little --encoding signed-integer -c 2 -t raw - -r 16000 --bits 16 --endian little --encoding signed-integer -c 1 -t raw -'.split(" ")
                        p = subprocess.Popen(soxcmd, stdout=subprocess.PIPE, stdin=subprocess.PIPE, stderr=subprocess.STDOUT)

                        convertedOutput = p.communicate(input=soxInput)[0]

                        #print("Converted length: " + str(len(convertedOutput)))
                        for i in range(blocksAvailable):
                            self.blockStack.append(convertedOutput[:160 * 2])
                            convertedOutput = convertedOutput[160 * 2:]

                except Exception as e:
                    print(e, flush = True)
                    traceback.print_exc()

            block = self.blockStack.pop(0)
            last_good_block_time = time.time()
            is_speech = self.vad.is_speech(block, 16000)

            #print(f"Block {block[:16]}... is speech: {is_speech} triggered: {triggered}", flush = True)

            # Returns False if nothing happens, None if end of phrase - ugh
            if not triggered:
                # Between phrases
                ring_buffer.append((block, is_speech))
                num_voiced = len([1 for (_, speech) in ring_buffer_recent_slice(num_start_window_blocks) if speech])
                if num_voiced >= (num_start_window_blocks * ratio):
                    # Start of phrase
                    triggered = True
                    for block, _ in ring_buffer_recent_slice(num_start_padding_blocks + num_start_window_blocks):
                        # print('|' if is_speech else '.', end='')
                        # print('|' if in_complex_phrase else '.', end='')
                        in_complex_phrase = yield block
                    # print('#', end='')
                    ring_buffer.clear()

                # Stop paying attention if there's too much silence and no phrase has been started yet
                num_unvoiced = len([1 for (_, speech) in ring_buffer_recent_slice(num_end_window_blocks) if not speech])
                if (num_unvoiced >= (num_end_window_blocks * ratio)):
                    ring_buffer.clear()
                    #print("DETECTED SILENCE.", flush = True)
                    in_complex_phrase = yield False

            else:
                # Ongoing phrase
                in_complex_phrase = yield block
                # print('|' if is_speech else '.', end='')
                # print('|' if in_complex_phrase else '.', end='')
                ring_buffer.append((block, is_speech))
                num_unvoiced = len([1 for (_, speech) in ring_buffer_recent_slice(num_end_window_blocks) if not speech])
                num_complex_unvoiced = len([1 for (_, speech) in ring_buffer_recent_slice(num_complex_end_window_blocks) if not speech])
                if (not in_complex_phrase and num_unvoiced >= (num_end_window_blocks * ratio)) or \
                    (in_complex_phrase and num_complex_unvoiced >= (num_complex_end_window_blocks * ratio)):
                    # End of phrase
                    triggered = False
                    ring_buffer.clear()

                    #print("END PHRASE.", flush = True)
                    in_complex_phrase = yield None

class KaldiRecognizer():
    def __init__(self):
        self.in_phrase = False

        kaldi_active_grammar.disable_donation_message()

        model_dir = None # Default
        tmp_dir = None # Default
        self.compiler = kaldi_active_grammar.Compiler(model_dir=model_dir, tmp_dir=tmp_dir)
        # compiler.fst_cache.invalidate()

        fst = self.compiler.compile_top_fst()
        dictation_fst_file = self.compiler.dictation_fst_filepath
        self.decoder = kaldi_active_grammar.KaldiAgfNNet3Decoder(model_dir=self.compiler.model_dir, tmp_dir=self.compiler.tmp_dir,
            top_fst=fst.fst_wrapper, dictation_fst_file=dictation_fst_file, save_adaptation_state=False,
            config={},)
        self.compiler.decoder = self.decoder

        ##### Set up a rule
        rule = kaldi_active_grammar.KaldiRule(self.compiler, 'TestRule')
        fst = rule.fst

        dictation_nonterm = '#nonterm:dictation'
        end_nonterm = '#nonterm:end'

        # Optional preface
        previous_state = fst.add_state(initial=True)
        next_state = fst.add_state()
        fst.add_arc(previous_state, next_state, 'computer')
        fst.add_arc(previous_state, next_state, None)  # Optionally skip, with an epsilon (silent) arc

        # Required free dictation
        previous_state = next_state
        extra_state = fst.add_state()
        next_state = fst.add_state()
        # These two arcs together (always use together) will recognize one or more words of free dictation (but not zero):
        fst.add_arc(previous_state, extra_state, dictation_nonterm)
        fst.add_arc(extra_state, next_state, None, end_nonterm)

        # Loop repetition, alternating between a group of alternatives and more free dictation
        previous_state = next_state
        next_state = fst.add_state()
        for word in ['period', 'comma', 'colon']:
            fst.add_arc(previous_state, next_state, word)
        extra_state = fst.add_state()
        next_state = fst.add_state()
        fst.add_arc(next_state, extra_state, dictation_nonterm)
        fst.add_arc(extra_state, next_state, None, end_nonterm)
        fst.add_arc(next_state, previous_state, None)  # Loop back, with an epsilon (silent) arc
        fst.add_arc(previous_state, next_state, None)  # Optionally skip, with an epsilon (silent) arc

        # Finish up
        final_state = fst.add_state(final=True)
        fst.add_arc(next_state, final_state, None)

        rule.compile()
        rule.load()

    def processBlock(self, block):
        if (block is False):
            return ""

        if (block is not None):
            if not self.in_phrase:
                # Start of phrase
                kaldi_rules_activity = [True]  # A bool for each rule
                self.in_phrase = True
            else:
                # Ongoing phrase
                kaldi_rules_activity = None  # Irrelevant

            self.decoder.decode(block, False, kaldi_rules_activity)
            output, info = self.decoder.get_output()
            #print("Partial phrase: %r" % (output,))
            recognized_rule, words, words_are_dictation_mask, in_dictation = self.compiler.parse_partial_output(output)

            return ' '.join(words)
        else:
            # End of phrase
            self.decoder.decode(b'', True)
            output, info = self.decoder.get_output()
            expected_error_rate = info.get('expected_error_rate', float('nan'))
            confidence = info.get('confidence', float('nan'))

            recognized_rule, words, words_are_dictation_mask = self.compiler.parse_output(output)
            is_acceptable_recognition = bool(recognized_rule)
            parsed_output = ' '.join(words)
            #print("End of phrase: eer=%.2f conf=%.2f%s, rule %s, %r" %
            #    (expected_error_rate, confidence, (" [BAD]" if not is_acceptable_recognition else ""), recognized_rule, parsed_output))

            #if (is_acceptable_recognition):
            self.in_phrase = False

            return parsed_output

    def stop(self):
        try:
            self.decoder.decode(b'', True)
            output, info = self.decoder.get_output()
            self.in_phrase = False
        except Exception as e:
            print("Kaldi exception while running")
            print(e)

def audioDistributor():
    global userBuffers, currentUserBufferID, ignoredUsers

    while (True):
        try:
            data = sys.stdin.buffer.read(3840 + 18 + 1)
            if (data.startswith(b"COMMAND_STOP")):
                print("Received stop command... Clearing buffers.", flush = True)
                currentUserBufferID = None
                userBuffers = {}
                kr.stop()
                continue

            elif (data.startswith(b"COMMAND_IGNORE_ID")):
                data = data[data.find(b" ") + 1:]
                data = data[:data.find(b"\0")]

                print(b"COMMAND_IGNORE_ID/ " + data, flush = True)
                ignoredUsers.append(data)
                
                if (data in userBuffers):
                    userBuffers[data].clearBuffer()

                if (currentUserBufferID == data):
                    print("COMMAND_IGNORE_ID, Ingoring current user.")
                    currentUserBufferID = None

                continue

            elif (data.startswith(b"COMMAND_CLEAR_IGNORE")):
                print("Clearing the banned user list...", flush = True)
                ignoredUsers = []
                continue

            delimiter = data.find(b'\0')
            uid = data[:delimiter]
            data = data[delimiter + 1:]

            if (uid in ignoredUsers):
                continue

            if (uid not in userBuffers):
                userBuffers[uid] = UserBuffer()

            userBuffers[uid].appendData(data)

        except Exception as e:
            print(e, flush = True)
            traceback.print_exc()

print("Recognizer starting...", flush = True)

ignoredUsers = []
userBuffers = {}

thr = threading.Thread(target = audioDistributor)
thr.daemon = True
thr.start()


currentUserBufferID = None
debugOldOutputs = {}
debugFocusingDelay = 0

kr = KaldiRecognizer()
print("Started Kaldi Recognizer.", flush = True)

ind = 0
ind2 = 0

maxind = 400000

while True:
    ind += 1
    if (ind % maxind == 0):
        buffer = "Python listening to/ " + str(currentUserBufferID) + "\n"
        for uid in list(userBuffers):
            buffer += f"{uid}/ {userBuffers[uid].getBufferSize()}\n"
            
        print(buffer, end = "", flush = True)

    if (currentUserBufferID == None):
        maxind = 200000
        maxUserBufferSize = 0

        # The max might change while we iterate, but it's not that big of an issue
        for ub in list(userBuffers):
            if (ub in ignoredUsers):
                continue

            if (userBuffers[ub].getBufferSize() > maxUserBufferSize):
                maxUserBufferSize = userBuffers[ub].getBufferSize()
                currentUserBufferID = ub

        if (currentUserBufferID != None):
            #print(f"{len(userBuffers)} user buffers.")
            #print("\n".join([str(uid) + ": " + str(userBuffers[uid].getBufferSize()) for uid in userBuffers]))

            #print(f"Now focusing on: {currentUserBufferID}", flush = True)
            debugFocusingDelay = 0
        elif (debugFocusingDelay == 0):
            #print(f"Not focusing on anyone.", flush = True)
            debugFocusingDelay = 1

    else:
        maxind = 150

        #print("Getting next block...", flush = True, end = " ")
        block = userBuffers[currentUserBufferID].getNextBlock()
        #print("OK", flush = True)

        #output = ""
        output = kr.processBlock(block)

        if (True or currentUserBufferID not in debugOldOutputs or output != debugOldOutputs[currentUserBufferID]):
            debugOldOutputs[currentUserBufferID] = output
            #print(f"Partial {currentUserBufferID}:{output}", flush = True)

        if (block == None or block == False):
            if (len(output) > 0):
                print(f"{currentUserBufferID.decode()}:{output}", flush = True)
                
            currentUserBufferID = None