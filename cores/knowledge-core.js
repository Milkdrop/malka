const https = require('https');
const sjson = require('secure-json-parse');

class ConversationalUnit {
	constructor(authorName) {
		this.IP = [];
		for (var i = 0; i < 4; i++) {
			this.IP.push(Math.floor(Math.random() * 255 + 1));
		}

		this.personalityCores = {
			GLADOS: {name: "GlaDOS",
					intro: "GLaDOS: `Hello and, again, welcome to the Aperture Science computer-aided enrichment center.`\n" +
							"USERNAME: `Oh... I am so dizzy...`\n" +
							"GLaDOS: `We hope your brief detention in the relaxation vault has been a pleasant one, your specimen has been processed and we are now ready to begin the test proper.`\n" +
							"USERNAME: `Ok. I'm ready`\n" +
							"GLaDOS: `Before we start, however, keep in mind that although fun and learning are the primary goals of all enrichment center activities, serious injuries may occur. For your own safety and the safety of others, please refrain from touching any of your surroundings`\n" +
							"USERNAME: `I see...`\n" +
							"GLaDOS: `I will now be monitoring you, while you go by your daily life. Refer to me if you need any help.`",
					resetExtra: "* USERNAME hard resets GLaDOS's memory system in order to forget what you all talked about. She is in clear pain. *"
			},

			GLADOS_ASK: {name: "GlaDOS",
					intro: "GLaDOS: `Hello and, again, welcome to the Aperture Science computer-aided enrichment center.`\n" +
							"USERNAME: `Oh... I am so dizzy...`\n" +
							"GLaDOS: `We hope your brief detention in the relaxation vault has been a pleasant one, your specimen has been processed and we are now ready to begin the test proper.`\n" +
							"USERNAME: `Ok. I'm ready`\n" +
							"GLaDOS: `Before we start, however, keep in mind that although fun and learning are the primary goals of all enrichment center activities, serious injuries may occur. For your own safety and the safety of others, please refrain from touching any of your surroundings`\n" +
							"USERNAME: `I see...`\n" +
							"GLaDOS: `I will now be monitoring you, while you go by your daily life. Refer to me if you need any help.`\n" +
							"USERNAME: `OK. GLaDOS, I have one question I need to ask.`\n" +
							"GLaDOS: `I am listening.`",
					resetExtra: "* USERNAME hard resets GLaDOS's memory system in order to forget what you all talked about. She is in clear pain. *"
			},

			CATGIRL: {name: "Malka",
					intro: "USERNAME: `Oh hi there! You are a cute small catgirl aren't you! Aww, you're soo cute.`\n" +
							"Malka: `Hello!!! I'm Malka! I'm a really cute and nice catgirl who loves everyone! omg!`\n" +
							"USERNAME: `Oh, I can see that lol, you're very energetic and fun to talk with, lol!`\n" +
							"Malka: `hihi! Yup! I love talking with a lot of exclamation marks!!! nya!`\n" +
							"USERNAME: `Okay, now let's starting having a normal conversation.`",
					resetExtra: "* USERNAME pounces Malka on the head so that she forgets everything you talked about. The poor catgirl is clearly confused, but is trying to recover quickly. *"
			},

			CATGIRL_ASK: {name: "Malka",
					intro: "USERNAME: `Oh hi there! You are a cute small catgirl aren't you! Aww, you're soo cute.`\n" +
							"Malka: `Hello!!! I'm Malka! I'm a really cute and nice catgirl who loves everyone! omg!`\n" +
							"USERNAME: `Oh, I can see that lol, you're very energetic and fun to talk with, lol!`\n" +
							"Malka: `hihi! Yup! I love talking with a lot of exclamation marks!!! nya!`\n" +
							"USERNAME: `Oh, by the way, may I ask you something?`\n" +
							"Malka: `Sure! Go ahead.`",
					resetExtra: "* USERNAME pounces Malka on the head so that she forgets everything you talked about. The poor catgirl is clearly confused, but is trying to recover quickly. *"
			}
		}

		this.setTemperature(0.85);
		this.setSize(250);
		this.setPersonalityCore("CATGIRL");
		this.setEngine("ELEUTHER");
		this.authorName = authorName;
		this.is_waiting_for_query = false;
	}

	changeIP() {
		for (var i = 3; i >= 0; i--) {
			this.IP[i]++;
			if (this.IP[i] <= 255) break;
			else {
				this.IP[i] = 1;
			}
		}
	}

	resetConversation(aware = false) {
		console.log("Resetting conversation...");
		this.conversation = this.personalityCore.intro.split('USERNAME').join(this.authorName);
		
		if (aware) {
			this.conversation += this.personalityCore.resetExtra.split('USERNAME').join(this.authorName);
		}

		this.conversation = this.conversation.trim() + "\n";
	}

	setPersonalityCore(personality) {
		personality = personality.toUpperCase();

		if (this.personalityCores[personality]) {
			this.personalityCore = this.personalityCores[personality];
			this.resetConversation();
			return true;
		} else {
			return false;
		}
	}

	setSize(size) {
		if (!isNaN(size) && size >= 10 && size <= 1000) {
			this.size = size;
			return true;
		} else {
			return false;
		}
	}

	setEngine(engine) {
		engine = engine.toUpperCase();

		if (["INFERKIT", "ELEUTHER"].includes(engine)) {
			this.engine = engine;
			return true;
		} else {
			return false;
		}
	}

	setTemperature(temperature) {
		if (!isNaN(temperature) && temperature >= 0.001 && temperature <= 100) {
			this.temperature = temperature;
			return true;
		} else {
			return false;
		}
	}

	async requestResponse(author, inputText, useOnlyInputText = false, retry_timeout = 1000, dont_append = false) {
 		this.oldConversation = this.conversation;

		if (this.is_waiting_for_query && this.engine == "ELEUTHER") {
			console.log("waiting for query...");
			return await new Promise((resolve, reject) => {
				setTimeout(() => {resolve(this.requestResponse(author, inputText, useOnlyInputText, retry_timeout, false))}, 500);
			});
		}

		this.is_waiting_for_query = true;

 		if (!useOnlyInputText && !dont_append)
			this.conversation += author + ': `' + inputText + '`\n' + this.personalityCore.name + ': `';

		var maxLength = 0;

		switch (this.engine) {
			case "INFERKIT": maxLength = 2750; break;
			case "ELEUTHER": maxLength = 16000; break;
		}
		
		if (this.conversation.length > maxLength) {
			this.conversation = this.conversation.substring(this.conversation.length - maxLength, this.conversation.length);
			this.conversation = this.conversation.substring(this.conversation.indexOf("\n") + 1);
			console.log("Trimming conversation... It now starts at:");
			console.log(this.conversation.substring(0, this.conversation.indexOf("\n")));
		}
		
		var data, options;

		switch (this.engine) {
			case "INFERKIT": {
				data = JSON.stringify({
					streamResponse: false,
					prompt: {
						text: useOnlyInputText ? inputText : this.conversation,
						isContinuation: false
					},
					startFromBeginning: false,
					length: this.size,
					forceNoEnd: false,
					topP: 0.9,
					temperature: this.temperature
				});

				options = {
					hostname: 'api.inferkit.com',
					port: 443,
					path: '/v1/models/standard/generate?useDemoCredits=true',
					method: 'POST',
					headers: {
						'X-Forwarded-For': this.IP.join("."),
						'User-Agent': 'Mozilla/5.0 (Windows NT 6.1; rv:10.0) Gecko/20100101 Firefox/10.0',
						'Content-Type': 'application/json charset=iso-8859-1',
						'Content-Length': Buffer.byteLength(data, 'utf8')
					}
				};
			} break;

			case "ELEUTHER": {
				data = JSON.stringify({
					context: useOnlyInputText ? inputText : this.conversation,
					top_p: 0.9,
					temp: this.temperature,
					response_length: this.size,
					remove_input: true
				});

				options = {
					hostname: 'api.eleuther.ai',
					port: 443,
					path: '/completion',
					method: 'POST',
					headers: {
						'X-Forwarded-For': this.IP.join("."),
						'User-Agent': 'Mozilla/5.0 (Windows NT 6.1; rv:10.0) Gecko/20100101 Firefox/10.0',
						'Content-Type': 'application/json',
						'Content-Length': Buffer.byteLength(data, 'utf8'),
						'Accept': 'application/json',
						'Referer': 'https://6b.eleuther.ai/',
						'Origin': 'https://6b.eleuther.ai'
					}
				};
			} break;

			default: console.log("Unknown engine: " + this.engine);
		}

		console.log(data);
		console.log("On: " + this.engine);

		return await new Promise((resolve, reject) => {
			const req = https.request(options, res => {
				console.log(`statusCode: ${res.statusCode}`);

				this.is_waiting_for_query = false;

				if (res.statusCode != 200) {
					console.log(req.error);
					console.log("Got status code != 200, retrying with another IP.");

					this.conversation = this.oldConversation;

					switch (this.engine) {
						case "INFERKIT": this.changeIP(); resolve(this.requestResponse(author, inputText, useOnlyInputText, retry_timeout, true)); break;
						case "ELEUTHER": this.changeIP(); setTimeout(() => {resolve(this.requestResponse(author, inputText, useOnlyInputText, retry_timeout + 1000, true))}, retry_timeout); break;
					}
				}

				let body = [];

				res.on('data', d => {
					body.push(d);
				}).on('end', () => {
					var outputText = "";

					try {
						var response = sjson.parse(Buffer.concat(body).toString());
						console.log(response)

						switch (this.engine) {
							case "INFERKIT": outputText = response.data.text.replace(/\n\n/g, "\n"); break;
							case "ELEUTHER": outputText = response[0].generated_text.replace(/\n\n/g, "\n"); break;
						}
						
					} catch (error) {
						console.error(error);
						console.log(Buffer.concat(body).toString());
					}

					console.log("received from GPT-2:\n" + outputText);

					if (outputText.length == 0) {
						console.log("Empty response. Waiting (hoping) that the status code wasn't 200.");
					} else {
						if (!useOnlyInputText) {
							var end = outputText.indexOf('`');

							if (outputText.indexOf('\n') < end) {
								end = outputText.indexOf('\n');
							}

							if (end != -1) {
								outputText = outputText.substring(0, end);
							}

							if (outputText.indexOf("'") == outputText.length - 1) {
								outputText = outputText.substring(0, outputText.length - 1);
							}

							this.conversation += outputText + '`\n';
						} else {
							outputText = "**" + inputText + "**" + outputText
						}

						resolve(outputText);
					}
				});
			});

			req.on('error', error => {
				console.error(error);
				reject(error);
			});
			
			req.write(data);
			req.end();
		});
	}
}

module.exports = ConversationalUnit;